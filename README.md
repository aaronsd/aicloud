# Spring Cloud proof-of-concept application

## 依赖版本
均为目前最新稳定版本
```
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.5.8.RELEASE</version>
		<relativePath /> <!-- lookup parent from repository -->
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<spring-cloud.version>Dalston.SR4</spring-cloud.version>
	</properties>
```

## 软件工具
-  **Spring Tool Suite™** 
    基于eclipse的Spring集成开发环境，特别是其Spring DashBoard管理服务启停非常实用； Spring starter project则能方便的创建springcloud工程，其功能与 http://start.spring.io/ 上的功能效果相同，但更好用。
-  **Postman**  
    Developing APIs is hard Postman makes it easy
-  **Atom**  
    非常好用的文本编辑器，插件很丰富


## master 分支
主要代码的分支，学习的内容主要在这个分支上进行增加和修改
## 其他分支
学习过程中遇到一些小分支的知识点，会创建一个分支进行修改
## tags
对master进行较大变化前会进行tag
## pom.xml
1. aicloud（爱cloud :sweat_smile: 学习工程随便取的名字）
2. aicloud-parent/pom.xml
约定将spring boot, spring cloud 的版本定义，环境参数定义，公共的基础依赖，均在此文件中定义

## eureka server
三个eureka server 两两相互注册组成一个高可用集群
首先要修改hosts文件映射
```
127.0.0.1    peer1 peer2 peer3
```
- peer1: [http://peer1:8761]([http://peer1:8761)
- peer2: [http://peer2:8762]([http://peer2:8762)
- peer3: [http://peer3:8763]([http://peer3:8763)
![输入图片说明](https://gitee.com/uploads/images/2017/1028/192414_00b7e6b8_722875.png "EUREKA.png")
## feign
默认的feign是不启用hystrix的，需要显示的在application.yml中设定
```
feign:
  hystrix:
    enabled: true
```
- master分支使用FallbackFactory来实现服务降级
- feign-fallback-by-class分支使用fallback class实现服务降级

## 注解@SpringCloudApplication
通过源码
```
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@SpringBootApplication
@EnableDiscoveryClient
@EnableCircuitBreaker
public @interface SpringCloudApplication {
}
```
可以发现SpringCloud的标准应用是应该包含服务发现和断路器。
另外通过测试发现@SpringCloudApplication还可以使得feign client生效也就是具有@EnableFeignClients的功能

## feign fallbackFactory 参数的坑
如果已存在定义好的feign client的降级服务类，如下:

//UserFeignClient.java
```
@FeignClient(name = "aicloud-users", configuration = FeignLogConfiguration.class, fallbackFactory = UserFeignClientFallBackFactory.class)
public interface UserFeignClient {

	@RequestMapping(value = "/user/{id}", method = RequestMethod.POST)
	public User findById(@PathVariable("id") Long id);
}
```
//UserFeignClientFallback.java
```
@Component
public class UserFeignClientFallback implements UserFeignClient {

	@Override
	public User findById(Long id) {
		User user = new User();
		user.setId(-1L);
		user.setName("feign降级服务");
		return user;
	}
}
```
如果启动类使用下面的注解，程序不会使用fallbackFactory，虽然是配置了，而是使用UserFeignClientFallback进行降级服务
```
@SpringCloudApplication
public class MoviesApplication
```
如果启动类使用下面的注解，程序才会使用fallbackFactory进行降级服务
```
@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
public class MoviesApplication
```
原因，，，我也想不明白……而如果不存在 UserFeignClientFallback.java 而使用@SpringCloudApplication注解的话程序启动会报错：
> Description:
>
>Field userFeignClient in com.aicloud.movies.controller.MovieControllerFeign required a bean of type 'com.aicloud.movies.feign.UserFeignClient' that could not be found.
>
>
>Action:
>
>Consider defining a bean of type 'com.aicloud.movies.feign.UserFeignClient' in your configuration.

## dashboard & turbine 
- dashboard: [http://localhost:8080/hystrix](http://localhost:8080/hystrix)
- turbine: [http://localhost:8989/turbine.stream](http://localhost:8989/turbine.stream)
![监控面板效果图](https://gitee.com/uploads/images/2017/1028/200902_4fd0bea3_722875.png "turbine.png")

## rabbitmq 聚合监控信息 (ashboard & turbine 合并为一个工程monitor)
![输用户名密码备忘](https://gitee.com/uploads/images/2017/1028/194505_48385c50_722875.png "ribbitMq.png")

![rabbitmq效果图](https://gitee.com/uploads/images/2017/1028/201028_011fcdc3_722875.png "ribbitMq.png")

- turbine stream address ：[http://localhost:8989/](http://localhost:8989/)
- dashboard: [http://localhost:8080/hystrix](http://localhost:8080/hystrix)
