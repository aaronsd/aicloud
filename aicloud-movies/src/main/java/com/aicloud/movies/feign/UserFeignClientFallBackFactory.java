package com.aicloud.movies.feign;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.aicloud.movies.entity.User;

import feign.hystrix.FallbackFactory;

@Component
public class UserFeignClientFallBackFactory implements FallbackFactory<UserFeignClient> {
	Logger logger = LoggerFactory.getLogger(UserFeignClientFallBackFactory.class);

	@Override
	public UserFeignClient create(Throwable cause) {
		return new UserFeignClient() {
			User user = new User();
			@Override
			public User post(User user) {
				user.setId(-1L);
				user.setName("fallbackFactory降级服务");
				logger.warn("fallabck, reason was " + cause);
				return user;
			}

			@Override
			public User get2(Map<String, Object> map) {
				user.setId(-1L);
				user.setName("fallbackFactory降级服务");
				logger.warn("fallabck, reason was " + cause);
				return user;
			}

			@Override
			public User get1(Long id, String username) {
				user.setId(-1L);
				user.setName("fallbackFactory降级服务");
				logger.warn("fallabck, reason was " + cause);
				return user;
			}

			@Override
			public User get0(User user) {
				user.setId(-1L);
				user.setName("fallbackFactory降级服务");
				logger.warn("fallabck, reason was " + cause);
				return user;
			}

			@Override
			public User findById(Long id) {
				user.setId(-1L);
				user.setName("fallbackFactory降级服务");
				logger.warn("fallabck, reason was " + cause);
				return user;
			}
		};
	}

}
