package com.aicloud.movies.feign;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.aicloud.movies.entity.User;

@Component
public class UserFeignClientFallback implements UserFeignClient {

	@Override
	public User findById(Long id) {
		User user = new User();
		user.setId(-1L);
		user.setName("feign降级服务");
		return user;
	}

	@Override
	public User get0(User user) {
		user.setId(-1L);
		user.setName("feign降级服务");
		return user;
	}

	@Override
	public User get1(Long id, String username) {
		User user = new User();
		user.setId(-1L);
		user.setName("feign降级服务");
		return user;
	}

	@Override
	public User get2(Map<String, Object> map) {
		User user = new User();
		user.setId(-1L);
		user.setName("feign降级服务");
		return user;
	}

	@Override
	public User post(User user) {
		user.setId(-1L);
		user.setName("feign降级服务");
		return user;
	}

}
