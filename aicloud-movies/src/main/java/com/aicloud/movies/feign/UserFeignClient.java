package com.aicloud.movies.feign;

import java.util.Map;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.aicloud.config.FeignLogConfiguration;
import com.aicloud.movies.entity.User;

@FeignClient(name = "aicloud-users", configuration = FeignLogConfiguration.class, fallbackFactory = UserFeignClientFallBackFactory.class)
public interface UserFeignClient {

	@GetMapping(value = "/user/{id}")
	public User findById(@PathVariable("id") Long id);

	@GetMapping("/get")
	public User get0(User user);

	@GetMapping(value = "/get")
	public User get1(@RequestParam("id") Long id, @RequestParam("username") String username);

	@GetMapping(value = "/get")
	public User get2(@RequestParam Map<String, Object> map);

	@PostMapping(value = "/post")
	public User post(@RequestBody User user);
}
