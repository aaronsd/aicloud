/**
 * 2017年10月29日
 */
/**
 * 前置过滤器，请求调用前执行，鉴权，记录信息等
 * 
 * @author aaron 2017年10月29日
 */
package com.aicloud.gateway.filters.pre;